import {
  Controller,
  Get,
  Post,
  Body,
  Delete,
  UseGuards,
  Request,
} from '@nestjs/common';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { TasksService } from './tasks.service';

interface ICreateInput {
  title: string;
  description: string;
}

@Controller('tasks')
export class TasksController {
  constructor(private readonly tasksService: TasksService) {}

  @Post('create')
  @UseGuards(JwtAuthGuard)
  async create(
    @Request() req,
    @Body() input: ICreateInput,
  ): Promise<{ id: string }> {
    return this.tasksService.create(input, req.user.id);
  }

  @Delete('delete')
  @UseGuards(JwtAuthGuard)
  async remove(
    @Request() req,
    @Body('id') id: string,
  ): Promise<{ id: string }> {
    return this.tasksService.remove(id, req.user.id);
  }

  @Get('get')
  @UseGuards(JwtAuthGuard)
  async getAll(@Request() req) {
    return this.tasksService.findAll(req.user.id);
  }
}
