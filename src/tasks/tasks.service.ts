import { Injectable, HttpException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { Tasks } from './tasks.schema';

interface ICreateInput {
  title: string;
  description: string;
}

@Injectable()
export class TasksService {
  constructor(@InjectModel(Tasks.name) private tasksModel: Model<Tasks>) {}

  async create(input: ICreateInput, owner: string): Promise<{ id: string }> {
    const newTask = new this.tasksModel({
      owner: owner,
      title: input.title || '',
      description: input.description || '',
    });
    await newTask.save();
    return {
      id: newTask._id.toString(),
    };
  }

  async findAll(userId: string) {
    return this.tasksModel.find(
      {
        owner: userId,
      },
      {
        _id: 1,
        owner: 1,
        description: 1,
        title: 1,
      },
    );
  }

  async remove(id: string, owner: string): Promise<{ id: string }> {
    if (Types.ObjectId.isValid(id)) {
      const subject = await this.tasksModel.findOne({
        _id: id,
        owner,
      });
      if (!subject) {
        throw new HttpException('Is not an owner', 403);
      }
      await this.tasksModel.deleteOne({ _id: id });
      return {
        id,
      };
    } else {
      throw new HttpException('Wrong Id', 404);
    }
  }
}
