import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

@Schema()
export class Tasks {
  @Prop(String)
  owner: string;

  @Prop(String)
  title: string;

  @Prop(String)
  description: string;
}

export const TasksSchema = SchemaFactory.createForClass(Tasks);
