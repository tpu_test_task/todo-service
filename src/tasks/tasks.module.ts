import { TasksController } from './tasks.controller';
import { Tasks, TasksSchema } from './tasks.schema';
import { MongooseModule } from '@nestjs/mongoose';
import { TasksService } from './tasks.service';
import { Module } from '@nestjs/common';

@Module({
  imports: [
    MongooseModule.forRoot(process.env.DB_URL),
    MongooseModule.forFeature([{ name: Tasks.name, schema: TasksSchema }]),
  ],
  controllers: [TasksController],
  providers: [TasksService],
})
export class TasksModule {}
