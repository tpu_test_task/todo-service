import { TasksModule } from './tasks/tasks.module';
import { AuthModule } from './auth/auth.module';
import { Module } from '@nestjs/common';

@Module({
  imports: [TasksModule, AuthModule],
})
export class AppModule {}
