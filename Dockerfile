FROM node:19-bullseye-slim

WORKDIR /todo

COPY ./ ./

RUN npm install && npm run build

CMD ["npm", "start"]
